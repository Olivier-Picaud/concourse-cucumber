FROM ruby:2.3.7

RUN gem install cucumber
RUN gem install rspec

COPY assets/ /opt/resource
RUN chmod +x -R /opt/resource

WORKDIR /opt/resource
