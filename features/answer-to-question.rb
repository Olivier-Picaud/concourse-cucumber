require 'net/http'
require 'uri'


Given(/^I ask for the question 'what is your name'$/) do
  @queryParam="q=66fca940: what is your name";
end

When(/^I want a reply$/) do
  uriEncoded = URI.encode("http://plop.plop.plop:9999/extreme?"+@queryParam)
  uri = URI.parse(uriEncoded)
  @response = Net::HTTP.get_response(uri)
end

Then(/^I should get '(.*?)'$/) do |answer|
  expect(@response.body).to eq(answer)
end
